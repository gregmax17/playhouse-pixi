/**
 * Plays a sound loops as your backgroud music, it uses a {@link https://pixijs.io/pixi-sound/docs/index.html|Sound} instance. 
 * You can transition from one background music to another, and it will auto fade in out (default milliseconds is `600`).
 * 
 * This class is already internally instantiated.
 * @namespace Music
 * @property {String} id Current `id` of the song being played.
 * @property {Sound} soundObj Calls `PIXI.sound.play`, see {@link https://pixijs.io/pixi-sound/docs/index.html|Sound}.
 */
 class Music {
	id = '';
	soundObj = null;

	/**
	 * Plays and loops the sound. 
	 * @param {String} id The `id` should reference the same from your resources.
	 * @param {Number} [volume=1] The volume which the music will fade to when coming in.
	 * @param {Number} [fadeTime=600] The duration of the fading music (going out and coming in).
	 */
	play(id, volume, fadeTime) {
		if (this.id === id) {
			return;
		}

		this.id = id;

		if (this.soundObj) {
			this.fadeTo(0, fadeTime).wait(100).call(this._play, [id, volume, fadeTime], this);
		}
		else {
			this._play(id, volume, fadeTime);
		}
	}

	/**
	 * Called internally.
	 */
	_play(id, volume = 1, fadeTime) {
		let soundObj = PIXI.sound.find(id);
		if (soundObj) {
			this.soundObj = soundObj.play({ loop: true, volume: 0 });
			this.fadeTo(volume, fadeTime);
		}
	}

	/**
	 * Resumes the music.
	 * @return {Music}
	 */
	resume() {
		if (this.soundObj) {
			this.soundObj.paused = false;//setPaused(false);
		}
		return this;
	}

	/**
	 * Pauses the music.
	 * @return {Music}
	 */
	pause() {
		if (this.soundObj) {
			this.soundObj.paused = true;//setPaused(true);
		}
		return this;
	}

	/**
	 * @return {boolean}
	 */
	isPaused() {
		return this.soundObj ? this.soundObj.paused : false;
	}

	/**
	 * Stops the music and sets the `soundObj` instance to `null`.
	 * @param {Number} [fadeTime=0]
	 * @return {createjs.Tween} {@link https://createjs.com/docs/tweenjs/classes/Tween.html|CreateJS Tween}
	 */
	stop(fadeTime = 0) {
		return this.fadeTo(0, fadeTime).call(this.soundObj.stop, null, this.soundObj).call(this._stop, null, this);
	}

	/**
	 * Called internally.
	 */
	_stop() {
		this.id = null;
		this.soundObj = null;
	}

	/**
	 * Fades the music to the desired `volume` in milliseconds.
	 * @param {Number} volume 
	 * @param {Number} [fadeTime=600]
	 * @return {createjs.Tween} {@link https://createjs.com/docs/tweenjs/classes/Tween.html|CreateJS Tween}
	 */
	fadeTo(volume, fadeTime = 600) {
		return createjs.Tween.get(this.soundObj || { volume: -volume }, { override: true }).to({ volume: volume }, fadeTime);
	}
}

export default new Music();