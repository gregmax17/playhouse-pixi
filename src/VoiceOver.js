import Loader from "./Loader.js";

/**
 * Can be used to play a sequence of audio files for in game tutorials.
 * 
 * This class is already internally instantiated.
 * 
 * @example
 * // play one audio clip
 * VoiceOver.play("tutorial-step-1", () => {
 * 	console.log("tutorial step 1 voice is over");
 * });
 * 
 * // play a sequence of audio clips
 * VoiceOver.play(["step-1", "step-2"], () => {
 * 	console.log("tutorial steps are over");
 * });
 * 
 * let complicated = {
 * 	id: "step-2",
 * 	beginCallback: () => { this.someOtherFtn(); },
 * };
 * VoiceOver.play(["step-1", complicated]);
 * @namespace VoiceOver
 * @property {boolean} isPlaying 
 * @property {Number} index
 * @property {Sound} soundObj Repeatly calls `PIXI.sound.play`, see {@link https://pixijs.io/pixi-sound/docs/index.html|Sound}.
 * @property {Number} volume
 */
class VoiceOver extends PIXI.utils.EventEmitter {
	isPlaying = false;
	index = 0;
	sequence = null;
	current = null;
	callback = null;
	soundObj = null;
	animations = [];
	loader = new PIXI.Loader();
	_volume = 1;

	/**
	 * Plays an audio, or a sequence of audios. 
	 * Can pass a `String` of the `id` audio clip to play, or an `Array` of `id`s to play, or an `Object` or an `Array` of `Object`s.
	 * The `Object` can have several properties:
	 * @example
	 * let complex = [
	 * 	"some-id",
	 * 	{
	 * 		"id": "next-id",
	 * 		"beginCallback": () => { console.log("start of step 2 audio"); }
	 * 	},
	 * 	"third-audio",
	 * 	{
	 * 		"id": "dont-forget",
	 * 		"delay": 1200, // wait 1.2 seconds
	 * 		"endCallback": () => { console.log("will be called after this audio clip ends"); }
	 * 	},
	 * 	"final-audio-id"
	 * ];
	 * VoiceOver.play(complex, () => {
	 * 	console.log("All that is complete!");
	 * });
	 * @param {Array|String|Object} sequence 
	 * @param {String} sequence.id
	 * @param {Number} sequence.delay The time in milliseconds to delay playing the audio clip.
	 * @param {Function} sequence.beginCallback
	 * @param {Function} sequence.endCallback
	 * @param {Function} callback 
	 * @param {Array} params
	 * @return {VoiceOver}
	 */
	play(sequence, callback) {
		this.stop();

		this.sequence = sequence instanceof Array ? sequence : [sequence];
		this.callback = callback || null;

		this.playNext();

		return this;
	}

	playNext() {
		// get next in the sequence
		this.current = this.sequence[this.index++];

		// if it exists, prep the play
		if (this.current) {
			createjs.Tween.get(this, { override: true }).wait(typeof this.current === "object" ? this.current.delay || 1 : 1).call(this.prepVO, null, this);
		}
		// we are done
		else {
			this.stop(true);

			// done with the sequence
			this.emit("sequence-end", { type: "sequence-end" });
		}
	}

	/**
	 * Loads the clip via `id`
	 */
	prepVO() {
		let id = typeof this.current === "object" ? this.current.id : this.current;
		let asset = Loader.findOne("id", id);

		if (!asset) {
			console.warn(`VoiceOver: No Asset ID of "${id}" `);
		}
		else {
			this.emit("prep", {
				type: "prep",
				current: this.current
			});

			// stops loading any previous VO clips if loading
			Object.keys(this.loader.resources).forEach(resource => {
				let res = this.loader.resources[resource];
				if (res && res.isLoading) {
					res.abort();
				}
			});

			// load the asset
			if (!this.loader.resources[asset.id]) {
				this.loader.loading = false;

				// remove previous callbacks
				if (this.loader.onComplete) {
					this.loader.onComplete.detachAll();
				}

				// load it
				this.loader.add(asset.id, asset.src).load(() => {
					this.startVO();
				});
			}
			// already loaded, start it
			else {
				this.startVO();
			}
		}
	}

	/**
	 * Actually creates the VoiceOver sound object. Emits `start`.
	 */
	startVO() {
		let id = typeof this.current === "object" ? this.current.id : this.current;

		// 1 millisecond delay so we can attach events, haha
		this.soundObj = PIXI.sound.find(id);
		if (this.soundObj) {
			this.isPlaying = true;

			this.emit("start", {
				type: "start",
				current: this.current
			});

			if (this.index === 1) {
				this.emit("sequence-start", {
					type: "sequence-start",
					sequence: this.sequence
				});
			}

			if (typeof this.current === "object" && typeof this.current.beginCallback === "function") {
				this.current.beginCallback();
			}

			this.soundObj.play({ 
				volume: this.volume,
				complete: () => { 
					this.endVO(); 
				} 
			});
		}
	}

	/**
	 * Is called at the end of the VoiceOver clip. Emits `end`.
	 */
	endVO() {
		// a small catch just in case we are fading out
		if (!this.isPlaying) {
			return;
		}

		this.emit("end", {
			type: "end",
			current: this.current
		});

		// only if its a function
		if (typeof this.current === "object" && typeof this.current.endCallback === "function") {
			this.current.endCallback();
		}

		// play the next one
		if (this.soundObj) {
			this.playNext();
		}
	}

	/**
	 * Stops the current running sequence running.
	 * If `callCompleteCallbacks` is set to `true` it will call the the complete callback when this sequence started. Emits `stop`.
	 * @example
	 * VoiceOver.play(["step-1", "step-2", "step-3"], () => {
	 * 	console.log("now that the tut steps are done, set up the game!");
	 * });
	 * 
	 * // sometime before the sequence finishes, like if the user wants to skip the tutorial
	 * VoiceOver.stop(true);
	 * @param {Boolean} [callCompleteCallbacks=false] 
	 * @return {VoiceOver}
	 */
	 stop(callCompleteCallbacks = false) {
		createjs.Tween.removeTweens(this);

		this.emit("stop", { type: "stop" });

		if (this.soundObj) {
			this.soundObj.stop();
			createjs.Tween.removeTweens(this.soundObj);
		}

		this.soundObj = null;
		this.index = 0;
		this.isPlaying = false;

		let callback = this.callback;
		this.callback = null;

		if (callCompleteCallbacks && callback) {
			callback();
		}

		return this;
	}

	/**
	 * Fades the sequence out over time.
	 * See {@link VoiceOver|VoiceOver.stop} for reference on `callCompleteCallbacks`.
	 * @param {Number} [time=600] 
	 * @param {Boolean} [callCompleteCallbacks=false]
	 * @return {createjs.Tween} {@link https://createjs.com/docs/tweenjs/classes/Tween.html|CreateJS Tween}
	 */
	fadeOut(time = 600, callCompleteCallbacks = false) {
		this.isPlaying = false;
		createjs.Tween.removeTweens(this);

		// done with the sequence
		this.emit("sequence-end", { type: "sequence-end" });

		if (this.soundObj) {
			return createjs.Tween.get(this.soundObj, { override: true }).to({ volume: 0 }, time).call(this.stop, [callCompleteCallbacks], this);
		}
		else {
			return createjs.Tween.get({}).wait(time);
		}
	}

	/**
	 * Can be called manually, and is called internally when losing focus on the window.
	 * Resumes the current running sound.
	 * @return {VoiceOver}
	 */
	resume() {
		if (this.soundObj) {
			this.soundObj.paused = false;//setPaused(false);
		}
		return this;
	}

	/**
	 * Can be called manually, and is called internally when gaining focus on the window.
	 * Pauses the current running sound.
	 * @return {VoiceOver}
	 */
	pause() {
		if (this.soundObj) {
			this.soundObj.paused = true;//setPaused(true);
		}
		return this;
	}

	/**
	 * @return {boolean}
	 */
	isPaused() {
		return this.soundObj ? this.soundObj.paused : false;
	}

	/**
	 * Sets the volume for the VoiceOver being played
	 * @param {Number}
	 */
	set volume(value) {
		if (this.soundObj) {
			this.soundObj.volume = value;
		}
		this._volume = value;
	}

	/**
	 * Returns the current volume of the VoiceOver
	 * @return {Number}
	 */
	get volume() {
		return this._volume;
	}
}

export default new VoiceOver();