/**
 * Will use `window.localStorage` if available, will fall back to `document.cookie` if otherwise.
 * Recommended to set the `prefix` property so no overlapping of the same keys happen on the same domain.
 * 
 * This class is already internally instantiated.
 * 
 * @example
 * Session.prefix = "game-prefix-";
 * Session.set("some-var", "some-value");
 * Session.set("another-var", { "object": "is ok" });
 * @property {String} prefix
 * @property {Number} [expires=365] For cookies, number of days exist before cookie expires.
 * @property {Object} [use=window.localStorage] By default, it will use `window.localStorage`, you can set this property to `window.sessionStorage` if desired
 */
 class Session {
	prefix = '';
	exprites = 365;
	use = window.localStorage;

	/**
	 * Checks support if able to write to `window.localStorage` or `window.sessionStorage` if `Session.use`is set, otherwise will return false.
	 * This is called internally every time you call `Session.set`, `Session.get`, `Session.remove`, and `Session.clear`.
	 * {@link https://michalzalecki.com/why-using-localStorage-directly-is-a-bad-idea/}
	 * @return {Boolean}
	 */
	checkSupport() {
		// not using it
		if (!this.use) {
			return false;
		}

		try {
			let key = '_S_E_S_S_I_O_N_';
			this.use.setItem(key, key);
			this.use.removeItem(key);
			return true;
		}
		catch (e) {
			return false;
		}
	}

	/**
	 * Sets the item with the passed `value` in the `window.localStorage`, or in the cookie.
	 * @param {String} name 
	 * @param {String|Number|Array|Object} value `Object` types not available for cookies.
	 * @param {Number} days If omitted, will use `expires` property. For cookies only.
	 */
	set(name, value, days) {
		if (this.checkSupport()) {
			try {
				this.use.setItem(this.prefix + name, JSON.stringify(value));
			}
			catch (e) {
				if (e === QUOTA_EXCEEDED_ERR) {
					console.log('localStorage quota exceeded');
				}
			}
		}
		else {
			if (!days) {
				days = this.expires;
			}

			let date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));

			let expires = '; expires=' + date.toGMTString();

			document.cookie = this.prefix + name + '=' + value + expires + '; path=/';
		}
	}

	/**
	 * Returns the value with paired with the item by the matched `name`.
	 * @param {String} name 
	 * @return {*}
	 */
	get(name) {
		if (this.checkSupport()) {
			try {
				return JSON.parse(this.use.getItem(this.prefix + name));
			}
			catch (e) {
				return this.use.getItem(name);
			}
		}
		else {
			let eq = this.prefix + name + '=',
				ca = document.cookie.split(';'),
				i = 0,
				c;

			for (; i < ca.length; i++) {
				c = ca[i];

				while (c.charAt(0) === ' ') {
					c = c.substring(1, c.length);
				}

				if (c.indexOf(eq) === 0) {
					return c.substring(eq.length, c.length);
				}
			}
		}

		return null;
	}

	/**
	 * Remove an item by the `name`.
	 * @param {String} name 
	 */
	remove(name) {
		if (this.checkSupport()) {
			this.use.removeItem(this.prefix + name);
		}
		else {
			this.set(name, '', -1);
		}
	}

	/**
	 * Clears out the `window.localStorage` items or removes all the cookie value items.
	 */
	clear() {
		if (this.checkSupport()) {
			this.use.clear();
		}
		else {
			document.cookie.split(';').forEach(function (c) {
				document.cookie = c.replace(/^ +/, '').replace(/=.*/, '=;expires=' + (new Date()).toUTCString() + ';path=/');
			});
		}
	}
}

export default new Session();