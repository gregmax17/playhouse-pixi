/**
 * The Loader uses the shared resources instance to load assets. {@link https://pixijs.download/dev/docs/PIXI.Loader.html|Loader}
 * 
 * @example
 * Loader.addAssets([
 *  { "id": "bg-title", "src": "assets/images/title-background.jpg", "tags": ["title"] },
 *  { "id": "sprites-game", "src": "assets/images/sprites.json", "tags": ["title", "game"] }
 * ]);
 * 
 * This class is already internally instantiated.
 * @namespace Loader
 * @property {Array} assets the array of objects
 */
 class Loader {
    assets = [];

    /**
     * Pass an Array of objects with the properties "id", "src", and "tags"
     * @param {Array} assets 
     */
    addAssets(assets) {
        this.assets = this.assets.concat(assets);
    }

    /**
     * Returns the Array from the `assets` that matches the `key` and their `values`
     * @param {String} key 
     * @param {String|Number} value 
     * @returns 
     */
    findAll(key, value) {
        let found = [];
        
        found = found.concat(this.assets.filter(asset => {
            if (asset[key]) {
                if (typeof asset[key] === "string") {
                    return asset[key] === value;
                }
                else if (Array.isArray(asset[key])) {
                    return asset[key].indexOf(value) > -1;
                }
            }
        }));

        return found;
    }

    /**
     * Returns the first instance from the `assets` Array that matches the `key` and their respective `value`
     * @param {key} key 
     * @param {String|Number} value 
     * @returns 
     */
    findOne(key, value) {
        return this.findAll(key, value)[0] || null;
    }

    /**
     * 
     * @param {String} key the key to check from `assets` Array
     * @param {String|Number} value the value of the key to check
     * @param {Function} callback executes the function when the resources load
     */
    loadAll(key, value, callback) {
        let manifest = this.findAll(key, value);
        let loader = PIXI.Loader.shared;

        // add asset to the loader if it doesn't exist
        manifest.filter(asset => {
            return !loader.resources[asset.id];
        }).forEach(asset => {
            // add to loader
            loader.add(asset.id, asset.src);
        });

        // start loading passing in the callback
        loader.load(callback);
    }
}

export default new Loader();