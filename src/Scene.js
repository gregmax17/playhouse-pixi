import { App } from "./App.js";

class Scene extends PIXI.Container {
	static instance;
	screenX = 0.5;
	screenY = 0.5;

	constructor() {
		super();

		if (Scene.instance) {
			Scene.instance.destroy();
		}

		if (App.instance) {
			App.instance.stage.addChild(this);
		}

		Scene.instance = this;
	}

	start() {
		
	}

	end() {

	}

	tick() {
		if (this.screenX && App.instance) {
			this.x = App.instance.width * this.screenX;
		}
		if (this.screenY && App.instance) {
			this.y = App.instance.height * this.screenY;
		}
	}
}

export { Scene };