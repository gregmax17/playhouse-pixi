// when we loop, its in the App.js
PIXI.DisplayObject.prototype.tickEnabled = true;
PIXI.DisplayObject.prototype.tickChildren = true;
PIXI.DisplayObject.prototype.tick = null;

// some setters and getters for the display object
Object.defineProperties(PIXI.DisplayObject.prototype, {
	alpha: {
		get: function () {
			return this._alpha;
		},
		set: function (alpha) {
			this._alpha = alpha;
			this.visible = this._alpha > 0; // this will disable mouse interactions
		}
	},
	scaleX: {
		get: function () {
			return this.scale ? this.scale.x : 0
		},
		set: function (value) {
			if (this.scale) {
				this.scale.x = value;
			}
		}
	},
	scaleY: {
		get: function () {
			return this.scale ? this.scale.y : 0;
		},
		set: function (value) {
			if (this.scale) {
				this.scale.y = value;
			}
		}
	},
	anchorX: {
		get: function () {
			return this.anchor ? this.anchor.x : 0;
		},
		set: function (value) {
			if (this.anchor) {
				this.anchor.x = value;
			}
		}
	},
	anchorY: {
		get: function () {
			return this.anchor ? this.anchor.y : 0;
		},
		set: function (value) {
			if (this.anchor) {
				this.anchor.y = value;
			}
		}
	},
	pivotX: {
		get: function () {
			return this.pivot ? this.pivot.x : 0;
		},
		set: function (value) {
			if (this.pivot) {
				this.pivot.x = value;
			}
		}
	},
	pivotY: {
		get: function () {
			return this.pivot ? this.pivot.y : 0;
		},
		set: function (value) {
			if (this.pivot) {
				this.pivot.y = value;
			}
		}
	},
	skewX: {
		get: function () {
			return this.skew ? this.skew.x : 0;
		},
		set: function (value) {
			if (this.skew) {
				this.skew.x = value;
			}
		}
	},
	skewY: {
		get: function () {
			return this.skew ? this.skew.y : 0;
		},
		set: function (value) {
			if (this.skew) {
				this.skew.y = value;
			}
		}
	}
});

// idea comes from createjs
[PIXI.utils.EventEmitter.prototype, PIXI.TextStyle.prototype].forEach(prototype => {
    if (prototype.set) {
        return;
    }

    prototype.set = function(props) {
        for (let prop in props) {
            this[prop] = props[prop];
        }
        return this;
    };
});