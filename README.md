# Playhouse PIXI #

A Web Game framework that utilizes PIXI.

### Dependencies and Versions Used ###
**PIXI 6.1.0**
* Min - https://pixijs.download/v6.1.0/pixi.min.js
* Docs - https://pixijs.download/v6.1.0/docs/index.html
* Releases - https://github.com/pixijs/pixijs/releases

**PIXI Sound 4.0.4**
* Min - https://sound.pixijs.download/v4.0.4/pixi-sound.js
* Docs - https://sound.pixijs.download/v4.0.4/docs/index.html
* Releases - https://github.com/pixijs/sound/releases

**TweenJS 1.0.0**
* Min - https://code.createjs.com/1.0.0/tweenjs.min.js
* Docs - https://www.createjs.com/docs/tweenjs/modules/TweenJS.html
* Releases - https://code.createjs.com/