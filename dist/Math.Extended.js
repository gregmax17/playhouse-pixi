"use strict";

/**
 * x is to istart and istop segment.
 * @#example Math.map(Math.random(), 0, 1, 10, 20); will provided a number between 10 and 20
 * @#param {Number} x - Value we are testing
 * @#param {Number} istart - Min value to test
 * @#param {Number} istop - Max value to test
 * @#param {Number} ostart - Min value to map to (based on x value)
 * @#param {Number} ostop - Max value to map to (based on x value)
 * @#return {Number} The number that is in the same relation to ostart and ostop segment
 */
Math.map = function (x, istart, istop, ostart, ostop) {
  return ostart + (ostop - ostart) * ((x - istart) / (istop - istart));
};
/**
 * Clamp a value between a min/max value
 * @#param {Number} x - Value we need to limit
 * @#param {Number} min - Min number to return
 * @#param {Number} max - Max number to return
 * @#return {Number} Either x, or the nearest limit if x is out of bounds.
 */


Math.clamp = function (x, min, max) {
  return Math.min(max, Math.max(min, x));
};
/**
 * @#example Math.roundAt(5.12345, 2) == 5.12  
 * @#example Math.roundAt(41624, -3) ==  42000
 * @#param {Number} x - Value we want to round
 * @#param {Number} precision - How precise do we want the rounding to be
 * @#return {Number} x rounded with the provided number of figures after the coma.
 */


Math.roundAt = function (x, precision) {
  if (!precision) {
    return Math.round(x);
  }

  precision = Math.pow(10, precision || 0);
  return Math.round(x * precision) / precision;
};
/**
 * If x > 0 returns the nearest lowest number, if x < 0, returns the nearest highest number
 * @#example toInt(-2.6) == -2
 * @#param {Number} x - Start value
 * @#return {Number} Integer that has the same integer part.
 */


Math.toInt = function (x) {
  return x | 0;
};
/**
 * Convert the angle, provided in degrees (0-360), into radians (0-2*PI)
 * @#param {Number} x - Angle in Degrees
 * @#return {Number} Angle in Radians
 */


Math.toRad = function (x) {
  return x * Math.PI / 180;
};
/**
 * Convert the angle, provided in radians (0-2*PI), into degrees (0-360)
 * @#param {Number} x - Angle in Radians
 * @#return {Number} Angle in Degrees
 */


Math.toDeg = function (x) {
  return x * 180 / Math.PI;
};
/**
 * Get random Int between range of numbers
 * @#param {Number} min - Min range value
 * @#param {Number} max - Max range value
 * @#return {Number} Return random Int value between min and max range
 */


Math.randomInt = function (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};
/**
 * Get random Float value between range of numbers
 * @#param {Number} min - Min range value
 * @#param {Number} max - Max range value
 * @#return {Number} Return random Float value between min and max range
 */


Math.randomFloat = function (min, max) {
  return Math.random() * (max - min) + min;
};
/**
 * Return is the provided number less then the difference between 1 and the smalling floating point number greater than 1
 * @#param {Number} number - Value to test
 * @#return {Boolean} Value less then EPSILON or not 
 */


Math.isEpsilon = function (number) {
  return Math.abs(number) < Number.EPSILON;
};
/**
 * Distance between two vectors
 * @#param {Vector2} a - Vector 1 {x: y:}
 * @#param {Vector2} b - Vector 2 {x: y:}
 * @#return {Number} Distance between vectors   
 */


Math.distance = function (a, b) {
  var dx = b.x - a.x;
  var dy = b.y - a.y;
  return Math.sqrt(dx * dx + dy * dy);
};
/**
 * Drop any fraction value
 * @#example 3.14 returns 3;
 * @#param {Number} x - Floating Point Number
 * @#return {Number} Number with no fractional digits
 */


Math.trunc = function (x) {
  var n = x - x % 1;
  return n === 0 && (x < 0 || x === 0 && 1 / x !== 1 / 0) ? -0 : n;
};
/**
 * Lerp between current position to target position over a period of time
 * @#param {Number} current - Current position value
 * @#param {Number} target - Target position value
 * @#param {Number} time - Time value
 * @#return {Number} Value between current/target over the time
 */


Math.lerp = function (current, target, time) {
  return current + time * (target - current);
};