"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * The Loader uses the shared resources instance to load assets. {@link https://pixijs.download/dev/docs/PIXI.Loader.html|Loader}
 * 
 * @example
 * Loader.addAssets([
 *  { "id": "bg-title", "src": "assets/images/title-background.jpg", "tags": ["title"] },
 *  { "id": "sprites-game", "src": "assets/images/sprites.json", "tags": ["title", "game"] }
 * ]);
 * 
 * This class is already internally instantiated.
 * @namespace Loader
 * @property {Array} assets the array of objects
 */
var Loader = /*#__PURE__*/function () {
  function Loader() {
    _classCallCheck(this, Loader);

    _defineProperty(this, "assets", []);
  }

  _createClass(Loader, [{
    key: "addAssets",
    value:
    /**
     * Pass an Array of objects with the properties "id", "src", and "tags"
     * @param {Array} assets 
     */
    function addAssets(assets) {
      this.assets = this.assets.concat(assets);
    }
    /**
     * Returns the Array from the `assets` that matches the `key` and their `values`
     * @param {String} key 
     * @param {String|Number} value 
     * @returns 
     */

  }, {
    key: "findAll",
    value: function findAll(key, value) {
      var found = [];
      found = found.concat(this.assets.filter(function (asset) {
        if (asset[key]) {
          if (typeof asset[key] === "string") {
            return asset[key] === value;
          } else if (Array.isArray(asset[key])) {
            return asset[key].indexOf(value) > -1;
          }
        }
      }));
      return found;
    }
    /**
     * Returns the first instance from the `assets` Array that matches the `key` and their respective `value`
     * @param {key} key 
     * @param {String|Number} value 
     * @returns 
     */

  }, {
    key: "findOne",
    value: function findOne(key, value) {
      return this.findAll(key, value)[0] || null;
    }
    /**
     * 
     * @param {String} key the key to check from `assets` Array
     * @param {String|Number} value the value of the key to check
     * @param {Function} callback executes the function when the resources load
     */

  }, {
    key: "loadAll",
    value: function loadAll(key, value, callback) {
      var manifest = this.findAll(key, value);
      var loader = PIXI.Loader.shared; // add asset to the loader if it doesn't exist

      manifest.filter(function (asset) {
        return !loader.resources[asset.id];
      }).forEach(function (asset) {
        // add to loader
        loader.add(asset.id, asset.src);
      }); // start loading passing in the callback

      loader.load(callback);
    }
  }]);

  return Loader;
}();

var _default = new Loader();

exports["default"] = _default;