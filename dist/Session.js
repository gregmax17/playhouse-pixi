"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Will use `window.localStorage` if available, will fall back to `document.cookie` if otherwise.
 * Recommended to set the `prefix` property so no overlapping of the same keys happen on the same domain.
 * 
 * This class is already internally instantiated.
 * 
 * @example
 * Session.prefix = "game-prefix-";
 * Session.set("some-var", "some-value");
 * Session.set("another-var", { "object": "is ok" });
 * @property {String} prefix
 * @property {Number} [expires=365] For cookies, number of days exist before cookie expires.
 * @property {Object} [use=window.localStorage] By default, it will use `window.localStorage`, you can set this property to `window.sessionStorage` if desired
 */
var Session = /*#__PURE__*/function () {
  function Session() {
    _classCallCheck(this, Session);

    _defineProperty(this, "prefix", '');

    _defineProperty(this, "exprites", 365);

    _defineProperty(this, "use", window.localStorage);
  }

  _createClass(Session, [{
    key: "checkSupport",
    value:
    /**
     * Checks support if able to write to `window.localStorage` or `window.sessionStorage` if `Session.use`is set, otherwise will return false.
     * This is called internally every time you call `Session.set`, `Session.get`, `Session.remove`, and `Session.clear`.
     * {@link https://michalzalecki.com/why-using-localStorage-directly-is-a-bad-idea/}
     * @return {Boolean}
     */
    function checkSupport() {
      // not using it
      if (!this.use) {
        return false;
      }

      try {
        var key = '_S_E_S_S_I_O_N_';
        this.use.setItem(key, key);
        this.use.removeItem(key);
        return true;
      } catch (e) {
        return false;
      }
    }
    /**
     * Sets the item with the passed `value` in the `window.localStorage`, or in the cookie.
     * @param {String} name 
     * @param {String|Number|Array|Object} value `Object` types not available for cookies.
     * @param {Number} days If omitted, will use `expires` property. For cookies only.
     */

  }, {
    key: "set",
    value: function set(name, value, days) {
      if (this.checkSupport()) {
        try {
          this.use.setItem(this.prefix + name, JSON.stringify(value));
        } catch (e) {
          if (e === QUOTA_EXCEEDED_ERR) {
            console.log('localStorage quota exceeded');
          }
        }
      } else {
        if (!days) {
          days = this.expires;
        }

        var date = new Date();
        date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
        var expires = '; expires=' + date.toGMTString();
        document.cookie = this.prefix + name + '=' + value + expires + '; path=/';
      }
    }
    /**
     * Returns the value with paired with the item by the matched `name`.
     * @param {String} name 
     * @return {*}
     */

  }, {
    key: "get",
    value: function get(name) {
      if (this.checkSupport()) {
        try {
          return JSON.parse(this.use.getItem(this.prefix + name));
        } catch (e) {
          return this.use.getItem(name);
        }
      } else {
        var eq = this.prefix + name + '=',
            ca = document.cookie.split(';'),
            i = 0,
            c;

        for (; i < ca.length; i++) {
          c = ca[i];

          while (c.charAt(0) === ' ') {
            c = c.substring(1, c.length);
          }

          if (c.indexOf(eq) === 0) {
            return c.substring(eq.length, c.length);
          }
        }
      }

      return null;
    }
    /**
     * Remove an item by the `name`.
     * @param {String} name 
     */

  }, {
    key: "remove",
    value: function remove(name) {
      if (this.checkSupport()) {
        this.use.removeItem(this.prefix + name);
      } else {
        this.set(name, '', -1);
      }
    }
    /**
     * Clears out the `window.localStorage` items or removes all the cookie value items.
     */

  }, {
    key: "clear",
    value: function clear() {
      if (this.checkSupport()) {
        this.use.clear();
      } else {
        document.cookie.split(';').forEach(function (c) {
          document.cookie = c.replace(/^ +/, '').replace(/=.*/, '=;expires=' + new Date().toUTCString() + ';path=/');
        });
      }
    }
  }]);

  return Session;
}();

var _default = new Session();

exports["default"] = _default;