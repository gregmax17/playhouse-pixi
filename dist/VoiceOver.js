"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _Loader = _interopRequireDefault(require("./Loader.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Can be used to play a sequence of audio files for in game tutorials.
 * 
 * This class is already internally instantiated.
 * 
 * @example
 * // play one audio clip
 * VoiceOver.play("tutorial-step-1", () => {
 * 	console.log("tutorial step 1 voice is over");
 * });
 * 
 * // play a sequence of audio clips
 * VoiceOver.play(["step-1", "step-2"], () => {
 * 	console.log("tutorial steps are over");
 * });
 * 
 * let complicated = {
 * 	id: "step-2",
 * 	beginCallback: () => { this.someOtherFtn(); },
 * };
 * VoiceOver.play(["step-1", complicated]);
 * @namespace VoiceOver
 * @property {boolean} isPlaying 
 * @property {Number} index
 * @property {Sound} soundObj Repeatly calls `PIXI.sound.play`, see {@link https://pixijs.io/pixi-sound/docs/index.html|Sound}.
 * @property {Number} volume
 */
var VoiceOver = /*#__PURE__*/function (_PIXI$utils$EventEmit) {
  _inherits(VoiceOver, _PIXI$utils$EventEmit);

  var _super = _createSuper(VoiceOver);

  function VoiceOver() {
    var _this;

    _classCallCheck(this, VoiceOver);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "isPlaying", false);

    _defineProperty(_assertThisInitialized(_this), "index", 0);

    _defineProperty(_assertThisInitialized(_this), "sequence", null);

    _defineProperty(_assertThisInitialized(_this), "current", null);

    _defineProperty(_assertThisInitialized(_this), "callback", null);

    _defineProperty(_assertThisInitialized(_this), "soundObj", null);

    _defineProperty(_assertThisInitialized(_this), "animations", []);

    _defineProperty(_assertThisInitialized(_this), "loader", new PIXI.Loader());

    _defineProperty(_assertThisInitialized(_this), "_volume", 1);

    return _this;
  }

  _createClass(VoiceOver, [{
    key: "play",
    value:
    /**
     * Plays an audio, or a sequence of audios. 
     * Can pass a `String` of the `id` audio clip to play, or an `Array` of `id`s to play, or an `Object` or an `Array` of `Object`s.
     * The `Object` can have several properties:
     * @example
     * let complex = [
     * 	"some-id",
     * 	{
     * 		"id": "next-id",
     * 		"beginCallback": () => { console.log("start of step 2 audio"); }
     * 	},
     * 	"third-audio",
     * 	{
     * 		"id": "dont-forget",
     * 		"delay": 1200, // wait 1.2 seconds
     * 		"endCallback": () => { console.log("will be called after this audio clip ends"); }
     * 	},
     * 	"final-audio-id"
     * ];
     * VoiceOver.play(complex, () => {
     * 	console.log("All that is complete!");
     * });
     * @param {Array|String|Object} sequence 
     * @param {String} sequence.id
     * @param {Number} sequence.delay The time in milliseconds to delay playing the audio clip.
     * @param {Function} sequence.beginCallback
     * @param {Function} sequence.endCallback
     * @param {Function} callback 
     * @param {Array} params
     * @return {VoiceOver}
     */
    function play(sequence, callback) {
      this.stop();
      this.sequence = sequence instanceof Array ? sequence : [sequence];
      this.callback = callback || null;
      this.playNext();
      return this;
    }
  }, {
    key: "playNext",
    value: function playNext() {
      // get next in the sequence
      this.current = this.sequence[this.index++]; // if it exists, prep the play

      if (this.current) {
        createjs.Tween.get(this, {
          override: true
        }).wait(_typeof(this.current) === "object" ? this.current.delay || 1 : 1).call(this.prepVO, null, this);
      } // we are done
      else {
        this.stop(true); // done with the sequence

        this.emit("sequence-end", {
          type: "sequence-end"
        });
      }
    }
    /**
     * Loads the clip via `id`
     */

  }, {
    key: "prepVO",
    value: function prepVO() {
      var _this2 = this;

      var id = _typeof(this.current) === "object" ? this.current.id : this.current;

      var asset = _Loader["default"].findOne("id", id);

      if (!asset) {
        console.warn("VoiceOver: No Asset ID of \"".concat(id, "\" "));
      } else {
        this.emit("prep", {
          type: "prep",
          current: this.current
        }); // stops loading any previous VO clips if loading

        Object.keys(this.loader.resources).forEach(function (resource) {
          var res = _this2.loader.resources[resource];

          if (res && res.isLoading) {
            res.abort();
          }
        }); // load the asset

        if (!this.loader.resources[asset.id]) {
          this.loader.loading = false; // remove previous callbacks

          if (this.loader.onComplete) {
            this.loader.onComplete.detachAll();
          } // load it


          this.loader.add(asset.id, asset.src).load(function () {
            _this2.startVO();
          });
        } // already loaded, start it
        else {
          this.startVO();
        }
      }
    }
    /**
     * Actually creates the VoiceOver sound object. Emits `start`.
     */

  }, {
    key: "startVO",
    value: function startVO() {
      var _this3 = this;

      var id = _typeof(this.current) === "object" ? this.current.id : this.current; // 1 millisecond delay so we can attach events, haha

      this.soundObj = PIXI.sound.find(id);

      if (this.soundObj) {
        this.isPlaying = true;
        this.emit("start", {
          type: "start",
          current: this.current
        });

        if (this.index === 1) {
          this.emit("sequence-start", {
            type: "sequence-start",
            sequence: this.sequence
          });
        }

        if (_typeof(this.current) === "object" && typeof this.current.beginCallback === "function") {
          this.current.beginCallback();
        }

        this.soundObj.play({
          volume: this.volume,
          complete: function complete() {
            _this3.endVO();
          }
        });
      }
    }
    /**
     * Is called at the end of the VoiceOver clip. Emits `end`.
     */

  }, {
    key: "endVO",
    value: function endVO() {
      // a small catch just in case we are fading out
      if (!this.isPlaying) {
        return;
      }

      this.emit("end", {
        type: "end",
        current: this.current
      }); // only if its a function

      if (_typeof(this.current) === "object" && typeof this.current.endCallback === "function") {
        this.current.endCallback();
      } // play the next one


      if (this.soundObj) {
        this.playNext();
      }
    }
    /**
     * Stops the current running sequence running.
     * If `callCompleteCallbacks` is set to `true` it will call the the complete callback when this sequence started. Emits `stop`.
     * @example
     * VoiceOver.play(["step-1", "step-2", "step-3"], () => {
     * 	console.log("now that the tut steps are done, set up the game!");
     * });
     * 
     * // sometime before the sequence finishes, like if the user wants to skip the tutorial
     * VoiceOver.stop(true);
     * @param {Boolean} [callCompleteCallbacks=false] 
     * @return {VoiceOver}
     */

  }, {
    key: "stop",
    value: function stop() {
      var callCompleteCallbacks = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      createjs.Tween.removeTweens(this);
      this.emit("stop", {
        type: "stop"
      });

      if (this.soundObj) {
        this.soundObj.stop();
        createjs.Tween.removeTweens(this.soundObj);
      }

      this.soundObj = null;
      this.index = 0;
      this.isPlaying = false;
      var callback = this.callback;
      this.callback = null;

      if (callCompleteCallbacks && callback) {
        callback();
      }

      return this;
    }
    /**
     * Fades the sequence out over time.
     * See {@link VoiceOver|VoiceOver.stop} for reference on `callCompleteCallbacks`.
     * @param {Number} [time=600] 
     * @param {Boolean} [callCompleteCallbacks=false]
     * @return {createjs.Tween} {@link https://createjs.com/docs/tweenjs/classes/Tween.html|CreateJS Tween}
     */

  }, {
    key: "fadeOut",
    value: function fadeOut() {
      var time = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 600;
      var callCompleteCallbacks = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      this.isPlaying = false;
      createjs.Tween.removeTweens(this); // done with the sequence

      this.emit("sequence-end", {
        type: "sequence-end"
      });

      if (this.soundObj) {
        return createjs.Tween.get(this.soundObj, {
          override: true
        }).to({
          volume: 0
        }, time).call(this.stop, [callCompleteCallbacks], this);
      } else {
        return createjs.Tween.get({}).wait(time);
      }
    }
    /**
     * Can be called manually, and is called internally when losing focus on the window.
     * Resumes the current running sound.
     * @return {VoiceOver}
     */

  }, {
    key: "resume",
    value: function resume() {
      if (this.soundObj) {
        this.soundObj.paused = false; //setPaused(false);
      }

      return this;
    }
    /**
     * Can be called manually, and is called internally when gaining focus on the window.
     * Pauses the current running sound.
     * @return {VoiceOver}
     */

  }, {
    key: "pause",
    value: function pause() {
      if (this.soundObj) {
        this.soundObj.paused = true; //setPaused(true);
      }

      return this;
    }
    /**
     * @return {boolean}
     */

  }, {
    key: "isPaused",
    value: function isPaused() {
      return this.soundObj ? this.soundObj.paused : false;
    }
    /**
     * Sets the volume for the VoiceOver being played
     * @param {Number}
     */

  }, {
    key: "volume",
    get:
    /**
     * Returns the current volume of the VoiceOver
     * @return {Number}
     */
    function get() {
      return this._volume;
    },
    set: function set(value) {
      if (this.soundObj) {
        this.soundObj.volume = value;
      }

      this._volume = value;
    }
  }]);

  return VoiceOver;
}(PIXI.utils.EventEmitter);

var _default = new VoiceOver();

exports["default"] = _default;