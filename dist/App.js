"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.App = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var resizeInterval = 0;
var lastWidth = 0;
var lastHeight = 0;
/**
 * Sets up the basic elements, events, and stage to use.
 * @example
 * class MyApp extends App {
 *      constructor() {
 *          super({
 * 				"wrapper": "playhouse-wrapper",
 *				"canvas": "playhouse-stage",
 *				"dimensions": {
 *					"width": { "min": 1024, "max": 9999 },
 *					"height": { "min": 768, "max": 9999 }
 *				},
 * 			});
 * 
 * 			// set the assets to the Loader
 * 			Loader.addAssets(assets);
 * 
 * 			Loader.loadAll("tags", ["title", "game"], () => {
 * 				console.log("loaded the assests");
 * 			});
 *      }
 * }
 * @namespace App
 * @property {Object} config A reference to the config object passed when instantiating the Play.
 * @property {HTMLElement|String} config.wrapper The wrapper element canvas element.
 * @property {HTMLElement|String} config.canvas The canavs element or id of the canvas element.
 * @property {Object} config.dimensions The dimensions of the canvas (See example above ^).
 * @property {Boolean} [config.wrapperScale=true] Sets the wrapper style width and height to 100%, if `config.wrapper` was specified.
 * @property {Boolean} [config.autoStop=true] When losing focus on the window, it will mute the sounds, pause the Music, Voice Over, and destroy the Ticker. When gaining focus it will recreate the Ticker and resume all sounds.
 * @property {Boolean} [config.autoCenter=true] Applies `marginTop` and `marginLeft` styles to the canvas to keep it centered in the wrapper.
 * @property {Boolean} [config.preventDefault=true] Calls `event.preventDefault` on the `touchmove` event.
 * @property {Boolean} [config.autoFocus=true] Calls `window.focus`.
 * @property {HTMLElement|String} config.dom Scales this DOM Element with the canvas (used for Text, UI, etc).
 */

var App = /*#__PURE__*/function () {
  function App(config) {
    _classCallCheck(this, App);

    _defineProperty(this, "config", null);

    if (App.instance) {
      return App.instance;
    }

    App.instance = this;
    this.config = Object.assign({
      autoStop: true,
      wrapperScale: true,
      autoCenter: true,
      preventDefault: true,
      autoFocus: true
    }, config);
    this.setupPIXI();
    this.setupElements();
    this.setupEvents(); // fix size now before first tick

    this.updateSize(); // add our tick listener

    PIXI.Ticker.shared.add(this.update, this);
  }

  _createClass(App, [{
    key: "update",
    value: function update() {
      var delta = PIXI.Ticker.shared.elapsedMS; // resize

      resizeInterval += delta;

      if (resizeInterval >= 500) {
        resizeInterval = 0;
        this.updateSize();
      } // update the tween


      if (createjs && createjs.Tween) {
        // remove listeners
        createjs.Ticker.removeAllEventListeners(); // update

        createjs.Tween.tick(delta);
      } // update the container 


      this.updatePixi(this.stage); // render the stage

      this.renderer.render(this.stage);
    }
    /**
     * Called interally, called every 500 milliseconds.
     */

  }, {
    key: "updateSize",
    value: function updateSize() {
      var config = this.config,
          dimensions = config.dimensions,
          canvas = this.renderer.view,
          bounds = config.wrapper === document.body ? {
        width: window.innerWidth,
        height: window.innerHeight
      } : (config.wrapper || canvas).getBoundingClientRect(),
          scale = Math.min(bounds.width / dimensions.width.min, bounds.height / dimensions.height.min),
          newWidth = Math.min(bounds.width / scale, dimensions.width.max),
          newHeight = Math.min(bounds.height / scale, dimensions.height.max);

      if (lastWidth === bounds.width && lastHeight === bounds.height) {
        return;
      }

      lastWidth = bounds.width;
      lastHeight = bounds.height; // update the width and height

      this.width = canvas.width = newWidth;
      this.height = canvas.height = newHeight; // update the renderer

      this.renderer.resize(this.width, this.height); // scale the width and height of the css

      canvas.style.width = "".concat(newWidth * scale, "px");
      canvas.style.height = "".concat(newHeight * scale, "px"); // center the game with css margin

      if (config.autoCenter) {
        canvas.style.marginTop = "".concat((bounds.height - newHeight * scale) * 0.5, "px");
        canvas.style.marginLeft = "".concat((bounds.width - newWidth * scale) * 0.5, "px");
      } // now the dom, this should be centered via css
      // we are just changing the scale of it so it matches the game


      if (config.dom) {
        var domStyle = config.dom.style;
        config.dom.transform = domStyle['-moz-transform'] = domStyle['-webkit-transform'] = domStyle['-ms-transform'] = "scale(".concat(scale, ")");
      }
    }
    /**
     * Calls `update` on the DisplayObject and it's children, if allowed.
     * @param {DisplayObject} parent 
     */

  }, {
    key: "updatePixi",
    value: function updatePixi(parent) {
      if (parent.tickChildren) {
        for (var i = parent.children.length - 1; i >= 0; i--) {
          this.updatePixi(parent.children[i]);
        }
      }

      if (parent.tickEnabled && parent.tick) {
        parent.tick(PIXI.Ticker.shared);
      }
    }
    /**
     * Called internally, from the cosntructor.
     */

  }, {
    key: "setupPIXI",
    value: function setupPIXI() {
      this.stage = new PIXI.Container();
      var config = this.config;
      var view = typeof config.canvas === 'string' ? document.getElementById(config.canvas) : config.view;
      this.renderer = PIXI.autoDetectRenderer(Object.assign({
        view: view
      }, this.config));
    }
    /**
     * Called internally, from the constructor.
     * Gets reference to the wrapper and dom elements.
     */

  }, {
    key: "setupElements",
    value: function setupElements() {
      var config = this.config; // wrapper

      config.wrapper = typeof config.wrapper === 'string' ? document.getElementById(config.wrapper) : config.wrapper || document.body; // scale it?

      if (config.wrapperScale && config.wrapper !== document.body) {
        config.wrapper.style.width = '100%';
        config.wrapper.style.height = '100%';
      } // get the dom


      config.dom = typeof config.dom === 'string' ? document.getElementById(config.dom) : config.dom;
    }
    /**
        * Called internally, from the constructor.
        * Sets up the window's page show and hide events (focus, blur, visibility, etc).
        */

  }, {
    key: "setupEvents",
    value: function setupEvents() {
      var _this = this;

      var config = this.config;
      var hidden = null; // we don't like this

      document.addEventListener('touchmove', function (event) {
        if (config.preventDefault) {
          event.preventDefault();
        }
      });

      if (document.hidden !== undefined) {
        hidden = 'visibilitychange';
      } else {
        ['webkit', 'moz', 'ms'].forEach(function (prefix) {
          if (document["".concat(prefix, "Hidden")] !== undefined) {
            hidden = "".concat(prefix, "Hidden");
          }
        });
      }

      var onChange = function onChange(event) {
        if (document.hidden || event.type === 'pause') {
          if (config.autoStop) {
            _this.onPageHide();
          }
        } else {
          if (config.autoStop) {
            _this.onPageShow();
          }
        }
      };

      if (hidden) {
        document.addEventListener(hidden, onChange, false);
      }

      window.addEventListener('focus', function () {
        if (config.autoStop) {
          _this.onPageShow();
        }
      }, false);
      window.addEventListener('blur', function () {
        if (config.autoStop) {
          _this.onPageHide();
        }
      }, false); // auto focus on the window

      if (config.autoFocus) {
        window.focus();
      }
    }
    /**
        * Called internally.
        * Unmutes audio, resumes Music and Voice Over, recreates the Ticker.
        */

  }, {
    key: "onPageShow",
    value: function onPageShow() {
      if (PIXI.Ticker.shared.started) {
        return;
      }

      PIXI.Ticker.shared.start();
      PIXI.sound.resumeAll();
    }
    /**
     * Called internally.
     * Mutes audio, pauses Music and Voice Over, destroys the Ticker.
     */

  }, {
    key: "onPageHide",
    value: function onPageHide() {
      if (!PIXI.Ticker.shared.started) {
        return;
      }

      PIXI.Ticker.shared.stop();
      PIXI.sound.pauseAll();
    }
  }]);

  return App;
}();

exports.App = App;

_defineProperty(App, "instance", void 0);